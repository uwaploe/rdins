# INS Data Reader

Rdins reads data records from the MUST Inertial Navigation System and publishes them to a [NATS Streaming Server](https://nats.io/documentation/streaming/nats-streaming-intro/). The input records are in ae "User Defined" record format as described in section 6.2.12 of the *Inertial Labs INS Interface Control Document*.

## Data Format

The input record values are converted from scaled integers to floating point values and published in [Message Pack](https://msgpack.org/index.html) format as an associative array (map) with the following entries:

| **Name**   | **Type**        | **Description**                                     |
|:-----------|:----------------|:----------------------------------------------------|
| Tsec       | 8-byte integer  | Seconds since 1/1/1970 UTC                          |
| Tnsec      | 4-byte integer  | Nanoseconds of the second                           |
| ImuOffset  | 8-byte integer  | Time offset between PVT and IMU measurements (nsec) |
| Gyro       | 3-element array | Angular rates (deg/s)                               |
| Accel      | 3-element array | Accelerations (g = 9.8106 m/s^2)                               |
| GyroBias   | 3-element array | Gyro angle biases                                   |
| AccelBias  | 3-element array | Accelerometer biases                                |
| Mag        | 3-element array | Raw magnetometer values (nT)                        |
| Usw        | 2-byte integer  | Status word                                         |
| Temp       | 4-byte float    | Internal temperature (degC)                         |
| Heading    | 4-byte float    | Vehicle heading (deg)                               |
| Pitch      | 4-byte float    | Vehicle rotation about the X-axis (deg)             |
| Roll       | 4-byte float    | Vehicle rotation about the Y-axis (deg)             |
| Lat        | 8-byte float    | Latitude (deg)                                      |
| Lon        | 8-byte float    | Longitude (deg)                                     |
| Alt        | 8-byte float    | Altitude (m)                                        |
| E          | 4-byte float    | Velocity in the East direction (m/s)                |
| N          | 4-byte float    | Velocity in the North direction (m/s)               |
| U          | 4-byte float    | Velocity in the up direction (m/s)                  |
| GnssLat    | 8-byte float    | Latitude from GNSS (deg)                            |
| GnssLon    | 8-byte float    | Longitude from GNSS (deg)                           |
| GnssAlt    | 8-byte float    | Altitude from GNSS (m)                              |
| GnssInfo   | 2-element array | GNSS data status words                              |
| SvInfo     | 1-byte integer  | Number of GNSS satellites tracked                   |
| NewGps     | 1-byte integer  | New update of GNSS data                             |
| NewAiding  | 2-byte integer  | Flag to indicate aiding data update                 |
| AidingData | map             | Aiding data from the DVL                            |

The *AidingData* map is described in the table below.

| **Name** | **Type**       | **Description**                     |
|----------|----------------|-------------------------------------|
| Vstbd    | 4-byte integer | Starboard velocity component (mm/s) |
| Vfwd     | 4-byte integer | Forward velocity component (mm/s)   |
| Vmast    | 4-byte integer | Up velocity component (mm/s)        |
| Estbd    | 2-byte integer | Vstbd error estimate (mm/s)         |
| Efwd     | 2-byte integer | Vfwd error estimate (mm/s)          |
| Emast    | 2-byte integer | Vmast error estimate (mm/s)         |
| Lat      | 2-byte integer | Measurement latency (ms)            |
| Pr       | 2-byte integer | Pressure (10Pa units)                                    |


### Notes

  * *Tsec* and *Tnsec* are the timestamp for the GNSS PVT solution.
  * A positive value for *ImuOffset* means the IMU was sampled after the GNSS.
  * *GnssInfo[0]*, *GnssInfo[1]*, and *NewGps* are all bit fields which are described in more detail in Tables 6.6, 6.7, and 6.8 of the *Inertial Labs INS Interface Control Document*.

## Parameters

Run-time parameters are read from environment variables.

| **Name**        | **Description**                             | **Required** |
|-----------------|---------------------------------------------|--------------|
| NATS_URL        | URL for NATS streaming server               | yes          |
| NATS_CLUSTER_ID | Server cluster ID                           | yes          |
| INS_ADDR        | Host:port for INS                           | yes          |
| INS_SUBJECT     | NATS subject for published data records     | yes          |
| INS_RAW_SUBJECT | NATS subject for published raw data records | no           |
| AIDING_SUBJECT  | NATS subject for aiding data                | no           |
| INS_RATE        | INS sampling rate in Hz                     | yes             |

## Raw Data

If the `INS_RAW_SUBJECT` variable is defined, publishing of the raw INS data records can be enabled and disabled via signals. Send `SIGUSR1` to the running to program to enable raw output and `SIGUSR2` to disable. The format of a raw data record is described in section 6.2.12 of the *Inertial Labs INS Interface Control Document*.
