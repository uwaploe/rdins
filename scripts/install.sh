#!/usr/bin/env bash

BINDIR="$HOME/bin"
SVCDIR="$HOME/.config/systemd/user"
PROGDIR="$(pwd)"

set -e
reqs=()
for req in "${reqs[@]}"; do
    type "$req"
done

# Install programs
mkdir -p $BINDIR
files=(*)
for f in "${files[@]}"; do
    # skip directories
    [[ -d $f ]] && continue
    # skip non-executable files
    [[ -x $f ]] || continue
    ln -v -s -f "$PROGDIR/$f" "$BINDIR"
done

mkdir -p $SVCDIR
if [[ -d systemd ]]; then
    cd systemd
    files=(*)
    for f in "${files[@]}"; do
        cp -av "$f" $SVCDIR
    done
    systemctl --user daemon-reload
fi
