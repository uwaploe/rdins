// Rdins reads data records from the inertial navigation system and
// publishes them to a NATS Streaming Server.
package main

import (
	"bytes"
	"context"
	"encoding/binary"
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"os"
	"os/signal"
	"runtime"
	"syscall"
	"time"

	ins "bitbucket.org/uwaploe/go-ins/v2"
	stan "github.com/nats-io/go-nats-streaming"
	"github.com/pkg/errors"
	"github.com/vmihailenco/msgpack"
)

const Usage = `Usage: rdins [options]

Read data from the MuST INS and publish to a NATS Streaming Server.
`

var Version = "dev"
var BuildDate = "unknown"

var (
	showVers = flag.Bool("version", false,
		"Show program version information and exit")
	natsURL    string = "nats://localhost:4222"
	clusterID  string = "must-cluster"
	insAddr    string
	insSubject string
	rawSubject string
	timeout    time.Duration = time.Second * 5
	insRate    int           = 0
)

func parseCmdLine() []string {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, Usage)
		flag.PrintDefaults()
	}

	flag.StringVar(&natsURL, "nats-url", lookupEnvOrString("NATS_URL", natsURL),
		"URL for NATS Streaming Server")
	flag.StringVar(&clusterID, "cid", lookupEnvOrString("NATS_CLUSTER_ID", clusterID),
		"NATS cluster ID")
	flag.StringVar(&insAddr, "ins-addr", lookupEnvOrString("INS_ADDR", insAddr),
		"HOST:PORT of the INS unit")
	flag.StringVar(&insSubject, "sub", lookupEnvOrString("INS_SUBJECT", insSubject),
		"Subject name for INS data")
	flag.StringVar(&rawSubject, "raw-sub", lookupEnvOrString("INS_RAW_SUBJECT", rawSubject),
		"Subject name for raw INS data")
	flag.DurationVar(&timeout, "timeout", lookupEnvOrDuration("INS_TIMEOUT", timeout),
		"Timeout for INS command response")
	flag.IntVar(&insRate, "rate", lookupEnvOrInt("INS_RATE", insRate),
		"INS sampling rate in Hz")
	flag.Parse()

	if *showVers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	return flag.Args()
}

func setInsRate(dev *ins.Device, rate uint) (p ins.Parameters, err error) {
	p, err = dev.Parameters()
	if err != nil {
		return p, errors.Wrap(err, "GetParameters")
	}

	p.Rate = rate

	err = dev.SetParameters(p)
	if err != nil {
		return p, errors.Wrap(err, "SetParameters")
	}
	time.Sleep(100 * time.Millisecond)

	return dev.Parameters()
}

func main() {
	parseCmdLine()

	if insAddr == "" {
		log.Fatal("INS address not specified, aborting")
	}

	conn, err := ins.NewConn(insAddr, timeout)
	if err != nil {
		log.Fatalf("open connection: %v", err)
	}
	defer conn.Close()

	sc, err := stan.Connect(clusterID, "ins-pub", stan.NatsURL(natsURL))
	if err != nil {
		log.Fatalf("Cannot connect: %v", err)
	}
	defer sc.Close()

	log.Printf("INS Reader: %s", Version)

	dev := ins.NewDevice(conn)

	// Initialize signal handler
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM,
		syscall.SIGHUP, syscall.SIGUSR1, syscall.SIGUSR2)
	defer signal.Stop(sigs)

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	var (
		align ins.Align
	)

	os.Setenv("INS_RAW_SUBJECT", rawSubject)
	// Goroutine to wait for signals or for the signal channel
	// to close.
	go func() {
		for {
			s, more := <-sigs
			if !more {
				return
			}
			// The value of rawSubject will be used to signal the
			// sampling loop as to whether the raw data should
			// be published.
			switch s {
			case syscall.SIGUSR1:
				rawSubject = os.Getenv("INS_RAW_SUBJECT")
				if rawSubject != "" {
					var b bytes.Buffer
					binary.Write(&b, ins.ByteOrder, align)
					sc.Publish(rawSubject, b.Bytes())
				}
			case syscall.SIGUSR2:
				rawSubject = ""
			default:
				log.Printf("Got signal: %v", s)
				cancel()
			}
		}
	}()

	// If a new sampling rate is requested, stop the INS and update the
	// parameters, otherwise we assume that the device has already been
	// started and is outputting OPVT & Raw IMU data records.
	if err == nil && insRate > 0 {
		// Take device out of continuous mode
		dev.Stop()
		// Delay a bit before sending a command
		time.Sleep(time.Second)

		info, err := dev.DevInfo()
		if err != nil {
			log.Fatalf("GetDevInfo: %v", err)
		}

		// Log the device info
		out, err := json.Marshal(info)
		if err == nil {
			log.Println(string(out))
		}

		log.Println("Setting INS sampling rate")
		params, err := setInsRate(dev, uint(insRate))
		if err != nil {
			log.Fatalf("Cannot set INS rate (%d): %v", insRate, err)
		}

		// Log the parameter settings
		out, err = json.Marshal(params)
		if err == nil {
			log.Println(string(out))
		}

		time.Sleep(1500 * time.Millisecond)
		align, err = dev.StartUserData()
		if err != nil {
			log.Fatalf("Cannot start INS: %v", err)
		}
		defer dev.Stop()

		// Log the alignment settings
		out, err = json.Marshal(align)
		if err == nil {
			log.Println(string(out))
		}
	}

	// Msgpack encode each data record and publish
	for payload := range dev.FilterRecords(ctx, uint8(ins.UserDefData)) {
		rec, err := ins.UnpackUserData(payload)
		if err != nil {
			log.Printf("Data unpacking failed: %v", err)
		} else {
			if rawSubject != "" {
				sc.Publish(rawSubject, payload)
			}
			b, err := msgpack.Marshal(&rec)
			if err != nil {
				log.Printf("Msgpack encoding failed: %v", err)
			} else {
				err = sc.Publish(insSubject, b)
				if err != nil {
					log.Printf("Publish failed: %v", err)
				}
			}
		}
	}

	log.Println("Data stream closed")
}
